{
    'name': 'To-Do Application',
    'description': 'Manage your personal Tasks with this module.',
    'author': '@jorgemustaine',
    'depends': ['base'],
    'data': ['security/ir.model.access.csv',
             'views/todo_menu.xml',
             'views/todo_view.xml',],
    'demo': ['demo/demo.xml'],
    'application': True,
}
