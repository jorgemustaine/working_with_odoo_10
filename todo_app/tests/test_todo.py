# -*- coding: utf-8 -*-


from odoo.exceptions import AccessError
from openerp.tests import common


class TestTodo(common.TransactionCase):

    def setUp(self):
        """Define the test environment.
        Here setting models and global variables"""
        super(TestTodo, self).setUp()
        user_demo = self.env.ref('base.user_demo')
        self.env = self.env(user=user_demo)
        # task with is_done False & active in True
        self.task_id_2 = self.env.ref('todo_app.Task2')

    def test_00_buttons_logic_toggle_done(self):
        """Add test to business logic"""

        # Test Toggle Done
        # First Check is_done is False
        self.assertFalse(self.task_id_2.is_done)
        self.task_id_2.do_toggle_done()
        # Check is_done is True
        self.assertTrue(self.task_id_2.is_done)

        # Test Clear Done
        # Check task is active
        self.assertTrue(self.task_id_2.active)
        self.task_id_2.do_clear_done()
        # Check task is not active
        self.assertFalse(self.task_id_2.active)

    def test_01_record_rule(self):
        """Test per user record rule"""
        todo = self.env['todo.task']
        task = todo.sudo().create({'name': 'Admin task'})
        with self.assertRaises(AccessError):
            todo.browse([task.id]).name
