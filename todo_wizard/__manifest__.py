{ 'name': 'To-Do Tasks Management Assistant',
  'description': 'Mass edit your To-Do backlog',
  'author': '@jorgescalona',
  'depends': ['todo_user'],
  'data': ['views/todo_wizard_view.xml'],
 }
