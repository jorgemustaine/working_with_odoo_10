# -*- coding:utf-8 -*-
from odoo import models, fields, api, exceptions
import logging

_logger = logging.getLogger(__name__)


class TodoWizard(models.TransientModel):
    _name = 'todo.wizard'
    _description = 'To-Do Mass assignment'
    task_ids = fields.Many2many('todo.task', string="Tasks")
    new_deadline = fields.Date('Deadline to Set')
    new_user_id = fields.Many2one('res.users', string='Responsible to Set')

    @api.multi
    def do_mass_update(self):
        self.ensure_one()
        if not (self.new_deadline or self.new_user_id):
            raise exceptions.ValidationError('No data to update!')
        _logger.debug('Mass update on Todo Tasks %s', self.task_ids.ids)
        vals = {}
        if self.new_deadline:
            vals['date_deadline'] = self.new_deadline
        if self.new_user_id:
            vals['user_id'] = self.new_user_id.id
        # Mass write values on all selected tasks
        if vals:
            self.task_ids.write(vals)
        return True

    @api.multi
    def _reopen_form(self):
        self.ensure_one()
        return {
            'type': 'ir.action.act_window',
            'res_model': self._name,  # this model
            'res_id': self.id,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }

    @api.multi
    def do_count_tasks(self):
        task = self.env['todo.task']
        count = task.search_count([('is_done', '=', False)])
        raise exceptions.Warning('There are %d active tasks. ' %count)

    @api.multi
    def do_populate_tasks(self):
        #  the web client not always supported a list because not use api.one
        #  with button action in this case it is pressable to use ensure_one()
        #  see documentation in:
    #  https://odoo-new-api-guide-line.readthedocs.io/en/latest/decorator.html
        self.ensure_one()
        task = self.env['todo.task']
        open_tasks = task.search([('is_done', '=', False)])
        #  Fill the wizard task list with all tasks
        self.task_ids = open_tasks
        #  reopen wizard form on same wizard record
        return self._reopen_form()

class TodoTask(models.Model):
    """Model inherit which Overriding create and write methods"""
    _inherit = 'todo.task'

    @api.model
    def create(self, vals):
        #  Code before create: can use the 'vals' dict
        new_record = super(TodoTask, self).create(vals)
        #  Code after create: can use 'new_record' created
        return new_record

    @api.multi
    def write(self, vals):
        #  Code before write: can use 'self' with the old values
        super(TodoTask, self).write(vals)
        #  Code after write: can use 'self' with the update values
        return True
