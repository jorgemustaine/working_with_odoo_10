# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.base.res.res_request import referenceable_models
from odoo.exceptions import ValidationError


class Tag(models.Model):
    """Model define tags for tasks"""
    _name = 'todo.task.tag'
    _description = 'To-Do Tag'
    name = fields.Char(size=40, translate=True)

    # Hierarchic relationships
    _parent_store = True

    _parent_name = 'parent_id'

    parent_id = fields.Many2one(
        'todo.task.tag', 'Parent Tag', ondelete='restrict',
    )
    parent_left = fields.Integer('Parent Left', index=True)
    parent_rigth = fields.Integer('Parent Rigth', index=True)

    child_ids = fields.One2many(
        'todo.task.tag', 'parent_id', 'Child Tags',
    )
    # Exercise add m2m to tasks
    task_ids = fields.Many2many('todo.task', String='Tasks')


class Stage(models.Model):
    """Model define Stages for tasks and declare different types of fields"""
    _name = 'todo.task.stage'
    _description = 'To-Do Stage'
    _order = 'sequence,name'
    # String fields:
    name = fields.Char(size=40, translate=True)
    desc = fields.Text('Description')
    state = fields.Selection(
        [('draft', 'New'), ('open', 'Started'),
         ('done', 'Closed')], 'State'
    )
    docs = fields.Html('Documentation')
    # Numeric fields:
    sequence = fields.Integer('Sequence')
    perc_complete = fields.Float('% Complete', (3, 2))
    # Date fields:
    date_effective = fields.Date('Effective Date')
    date_changed = fields.Datetime('Last Cahnged')
    # Other fields:
    fold = fields.Boolean('Folded?')
    image = fields.Binary('Image')
    # Exercise add O2m to tasks
    task_ids = fields.One2many('todo.task', 'stage_id',
                               String='Tasks in this stage')


class TodoTask(models.Model):
    """Model inherit which contains the relations between Tags and Stages"""
    _inherit = 'todo.task'
    # Relational fields
    stage_id = fields.Many2one('todo.task.stage', 'Stage')
    tag_ids = fields.Many2many(
        'todo.task.tag',  # related = (model name)
        'todo_task_tag_rel',  # relation= (table name)
        'task_id',  # column1= ("this" field)
        'tag_id',  # column2= ("other")
        string='Tags',
        # relational field attributes:
        auto_join=False, context={}, domain=[], ondelete='cascade',
    )
    # Dynamic reference field
    refers_to = fields.Reference(referenceable_models,
                                 'Refres To',)
    # Related Field
    state = fields.Selection(related='stage_id.state', string='Stege state',
                             store=True)
    # Computed fields
    stage_fold = fields.Boolean(
        'Stage Folded?', compute='_compute_stage_fold', store=False,
        search='_search_stage_fold', inverse='_inverse_write_stage_fold')

    effort_estimate = fields.Integer('Effort Estimated')

    # Python SQL constrains
    _sql_constrains = [
        ('todo_task_name_uniq',
         'UNIQUE (name, active)',
         'Task title must be unique!',)
    ]

    """ Python constraints can use a piece of arbitrary code to check the
    conditions. checking function should be decorated with @api.constraints ,
    indicating the list of fields involved in the check."""

    @api.constrains('name')
    def _check_name_size(self):
        for todo in self:
            if len(todo.name) < 5:
                raise ValidationError('Must have 5 chars!')

    @api.depends('stage_id.fold')
    def _compute_stage_fold(self):
        for task in self:
            task.stage_fold = task.stage_id.fold

    def _search_stage_fold(self, operator, value):
        return [('stage_id.fold', operator, value)]

    def _inverse_write_stage_fold(self):
        self.stage_id.fold = self.stage_fold

    def _compute_user_todo_count(self):
        for task in self:
            task.user_todo_count = task.search_count(
                [('user_id', '=', task.user_id.id)]
            )

    user_todo_count = fields.Integer(
        'User To-Do Count',
        compute='_compute_user_todo_count',
    )
